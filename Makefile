EXECUTABLE=httperf

build:
	go build -o $(EXECUTABLE) -ldflags "-w" main.go

clean:
	rm -f $(EXECUTABLE)
