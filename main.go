package main

import (
	"bufio"
	"flag"
	"fmt"
	"io"
	"log"
	"net"
	"net/http"
	"os"
	"os/signal"
	"path/filepath"
	"strconv"
	"strings"
	"sync"
	"syscall"
	"time"
)

const (
	CRLF                 = "\r\n"
	HTTP_HEADER_LINE     = "HTTP/1.1"
	CONTENT_LENGTH       = "Content-Length: "
	CONTENT_TYPE         = "Content-Type: application/octet-stream"
	MAX_TRANSACTION_TIME = int64(time.Minute)
)

var (
	addr            = ":8080"
	sample          = make([]byte, 256*1024)
	zero            = time.Time{}
	prefix          = "/test"
	myStreamMonitor *streamMonitor
	fs_root         string
)

func init() {
	flag.StringVar(&addr, "l", addr, "listen address")
	flag.StringVar(&prefix, "p", prefix, "test: the perfix of url path could be accepted")
	flag.Bool("help-description", false, "Will determine the server mode(Testing/FileServer) according to whether or not the first parameter is given.")
	flag.Parse()

	if flag.NArg() > 0 {
		fs_root = flag.Arg(0)
	}

	log.SetOutput(os.Stdout)
	myStreamMonitor = &streamMonitor{
		bucket: make(map[string]*stream),
		lock:   new(sync.Mutex),
	}
}

func waitSignal() {
	var sigChan = make(chan os.Signal, 1)
	signal.Notify(sigChan, syscall.SIGINT, syscall.SIGQUIT, syscall.SIGTERM)
	// Block until a signal is received.
	sig := <-sigChan
	log.Println("terminated by signal", sig)
}

func main() {
	if fs_root == "" {
		go myStreamMonitor.monitor()
		go runTestServer()
	} else {
		go runFileServer(fs_root)
	}
	waitSignal()
}

func runFileServer(dir string) {
	_, err := os.Stat(dir)
	abortIf(err)

	handler := http.FileServer(http.Dir(dir))
	serv := &server{fileHandler: handler}
	serv.Handler = serv

	ln, err := net.Listen("tcp", addr)
	abortIf(err)
	defer ln.Close()

	abspath, err := filepath.Abs(dir)
	if err != nil {
		abspath = dir
	}
	log.Println("FileServer listen at", ln.Addr())
	log.Println("root is", abspath)
	err = serv.Serve(ln)
	abortIf(err)
}

func runTestServer() {
	ln, err := net.Listen("tcp", addr)
	abortIf(err)
	defer ln.Close()
	log.Println("httperf is running at", ln.Addr())
	for {
		conn, err := ln.Accept()
		if err == nil {
			go testServlet(conn)
		} else {
			break
		}
	}
}

func testServlet(conn net.Conn) {
	defer conn.Close()
	req, e := http.ReadRequest(bufio.NewReader(conn))
	if e != nil || req.Method != "GET" {
		return
	}
	// incorrect prefix
	if !strings.HasPrefix(req.RequestURI, prefix) {
		return
	}
	var contentLen int
	if suffix := req.RequestURI[len(prefix):]; len(suffix) > 0 {
		var num int
		var flag bool
		for i, n := range suffix {
			if n >= 0x30 && n <= 0x39 {
				num = num*10 + int(n) - 0x30
				flag = true
			} else {
				flag = true
				switch n {
				case 'b', 'B':
				case 'k', 'K':
					num <<= 10
				case 'm', 'M':
					num <<= 20
				case '.', '_', '-', '!':
					if i == 0 {
						continue
					}
				default:
					flag = false
				}
				flag = flag && (i+1 == len(suffix))
				break
			}
		}
		if flag {
			contentLen = num
		} else {
			responseError(conn)
			return
		}
	}
	var rAddr = conn.RemoteAddr()
	s := myStreamMonitor.newStream(conn, rAddr)
	s.doTransaction(contentLen)
	myStreamMonitor.remove(rAddr)
}

func responseError(conn net.Conn) {
	conn.Write([]byte(HTTP_HEADER_LINE + " 404 Not found" + CRLF + CRLF))
}

type server struct {
	http.Server
	fileHandler http.Handler
}

func (s *server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	log.Printf("%s - %s", r.RemoteAddr, r.RequestURI)
	s.fileHandler.ServeHTTP(w, r)
}

type stream struct {
	net.Conn
	rAddr net.Addr
	birth int64
}

func (s *stream) doTransaction(contentLen int) {
	defer func() {
		_ = recover()
	}()
	s.Conn.SetDeadline(zero)
	if !s.writeHeader(contentLen) {
		return
	}
	log.Println("Testing from", s.rAddr)
	var nw int64
	var t0 = time.Now()
	if contentLen > 0 {
		nw = fillSteamLimited(s.Conn, contentLen)
	} else {
		nw = fillSteam(s.Conn)
	}
	printResults(nw, time.Now().Sub(t0), s.rAddr)
}

func (s *stream) writeHeader(contentLen int) bool {
	hBuf := make([]string, 3)
	hBuf[0] = HTTP_HEADER_LINE + " 200 OK"
	if contentLen > 0 {
		hBuf[1] = CONTENT_LENGTH + strconv.Itoa(contentLen)
		hBuf[2] = CONTENT_TYPE
	} else {
		hBuf[1] = CONTENT_TYPE
		hBuf = hBuf[:2]
	}
	// send headers
	_, e := s.Conn.Write([]byte(strings.Join(hBuf, CRLF) + CRLF + CRLF))
	return e == nil
}

type streamMonitor struct {
	bucket map[string]*stream
	lock   sync.Locker
}

func (m *streamMonitor) newStream(conn net.Conn, a net.Addr) *stream {
	s := &stream{conn, a, time.Now().UnixNano()}
	m.lock.Lock()
	m.bucket[a.String()] = s
	m.lock.Unlock()
	return s
}

func (m *streamMonitor) remove(a net.Addr) {
	m.lock.Lock()
	defer m.lock.Unlock()
	delete(m.bucket, a.String())
}

func (m *streamMonitor) monitor() {
	for t := range time.Tick(30 * time.Second) {
		m.lock.Lock()
		now := t.UnixNano()
		for k, v := range m.bucket {
			if now-v.birth > MAX_TRANSACTION_TIME {
				log.Printf("Abondon the connection %s because of living too long.\n", k)
				v.Close()
			}
		}
		m.lock.Unlock()
	}
}

func fillSteam(dst io.Writer) int64 {
	var c, n int
	var err error
	for ; ; c++ {
		n, err = dst.Write(sample)
		if err != nil {
			break
		}
	}
	return int64(c)*int64(len(sample)) + int64(n)
}

func fillSteamLimited(dst io.Writer, max int) int64 {
	var total, n int
	var err error
	for total < max {
		n, err = dst.Write(sample)
		total += n
		if err != nil {
			break
		}
	}
	return int64(total)
}

func printResults(t int64, elapses time.Duration, rAddr net.Addr) {
	seconds := elapses.Seconds()
	if seconds <= 1 {
		return
	}
	var speeds string
	var speed float64 = float64(t) / seconds
	var units = []string{"B", "K", "M", "G"}
	for _, u := range units {
		if speed < 1024 {
			speeds = fmt.Sprintf("%.2f%s", speed, u)
			break
		}
		speed /= 1024
	}
	log.Printf("Test from=%s Time=%.2fs  Speed=%s\n", rAddr, seconds, speeds)
}

func abortIf(e interface{}) {
	if e != nil {
		log.Fatalln(e)
	}
}
